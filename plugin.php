<?php
/*
Plugin Name: Create Salted Passwords
Plugin URI: http://code.otto-hanika.de/ao_yourls_salted_passwords
Description: Create salted passwords
Version: 1.0
Author: Andreas Otto
Author URI: http://code.otto-hanika.de/
License: GPL 3 or later
*/

// No direct call
if(!defined('YOURLS_ABSPATH')) die();

if(!defined('AO_YOURLS_SALTED_PASSWORDS_HASH_ALGO')) {
	define('AO_YOURLS_SALTED_PASSWORDS_HASH_ALGO', 'ripemd256');
}

// Register our plugin admin page
yourls_add_action('plugins_loaded', 'ao_yourls_salted_passwords_addPage');

function ao_yourls_salted_passwords_addPage() {
	yourls_register_plugin_page( 'salted_passwords', 'Create Salted Passwords', 'ao_yourls_salted_passwords_loadPage' );
}

function ao_yourls_salted_passwords_loadPage() {
	if($_POST['password']) {
		$password = $_POST['password'];
		$salt = substr(hash_hmac(AO_YOURLS_SALTED_PASSWORDS_HASH_ALGO, md5(uniqid(rand(), true)), md5(uniqid(rand(), true))), 0, 5);
		$encrypted = 'md5:' . $salt . ':' . md5( $salt . $password );
		printf('<p>Password entered: <strong>%s</strong></p>', $password);
		printf('<p>Encrypted password for a YOURLS config: <strong>%s</strong></p>', $encrypted);
	}
	printf('
	<form action="%s" accept-charset="UTF-8" method="post">
		<label for="password">Generate salted md5 hash for</label>
		<input id="password" name="password" type="password" />
		<input type="submit" value="Create" />
	</form>
	<p>This form just outputs hashes. Nothing is stored or logged.</p>
	',
		$_SERVER['php_self']
	);
}